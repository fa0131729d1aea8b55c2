### Keybase proof

I hereby claim:

  * I am MrBrax on github.
  * I am braxen (https://keybase.io/braxen) on keybase.
  * I have a public key whose fingerprint is 9482 D04A B5DA 6F51 66EC  E98B 6B14 65FD B3EC 049C

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "fingerprint": "9482d04ab5da6f5166ece98b6b1465fdb3ec049c",
            "host": "keybase.io",
            "key_id": "6b1465fdb3ec049c",
            "kid": "010124af918a5525c199e9b06cd653e89fe7537e3af3e9d09d721802eee01ca2c1930a",
            "uid": "fac171a57a59f24e5d4887710be3c719",
            "username": "braxen"
        },
        "service": {
            "name": "github",
            "username": "MrBrax"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1434203014,
    "expire_in": 157680000,
    "prev": "30909e942e2a5d0761a2524124271a586bc8e70aeb6183cd0d9d2dcaa151362d",
    "seqno": 2,
    "tag": "signature"
}
```

with the key [9482 D04A B5DA 6F51 66EC  E98B 6B14 65FD B3EC 049C](https://keybase.io/braxen), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.8
Comment: https://keybase.io/crypto

yMIPAnicbZJtSBRBGMfPtKyLIjKQVDBGrC93OTO7sy9CBYYvFVJoEYVwze7Onot1
p3t35msFQVIEEmFegVgfCiozQhO1MgutrCSwJHoRTELBMJKwkkqaFfsQNF+G+T+/
/595npm+FbEud0zrlrGZgoLyaMyzByTi2lMjnKgGWtCoBJnVoITNb6YV8DO71LYC
YZAJVFHBBhSpRgwqmQRJEtOZqmiShkSJmIYmMB2Kqg48oDgYchw8RqMhttEKco0f
fJbB1f/wJfMFiCDCIjVVpFBCMNGRqjJVg5JuSERgimoymQgyE6gpMNWAqiFjpEDM
GINIp5jzAqQ8LjIfZ1IdyYgSmRLVxCIjhqgosoygxgRdRqoDhpgdoIcYpzWbVrAA
qPUArpVbOnMGsFDzW+HiiPYvn29ncYfDhytLHeEw03wLVp9mBQw+O+4oZ3bICgZA
JuKkHrYcLxIFEUMBItEDWEWpZTOf5RBElhTIlweU2qycRwpQhXwCImaYEgPKEqKY
YJEPCTuNKZKmK0yGlGkSUgTdgIZqYEOnFBEkSNgATjNlgSDIxPya1M8jQ5Y/QMMR
m4Hah71Fca4Yt2vJ4kXO67vcy1b9/RNHfsbPeZvjL3fJ+euGmrzp+aM/GrvSQ/DU
/dzx096BDdj16KNn94fw4l+z+2ZKqpThqO9FFhbC20b73b3N6zPu1eSetFvH62sz
2m5kP02Z6Rj6cnHweeLE1qR319s/qcsHGoX6tea5brPNndD/5ODIdEJNwYU6b7Qr
pd9fHK3tevk6p+z85uORJP/eWBSX2jk1ciZ7cnS/u6kn+1Z6Yyfq7Zs9uqnxzmza
q6mGlkTSceDm1bNjXlBYONNRtTrP/JZ6bCpxMtm3zgu2R+eGcm9/zXl/6c2Opdd+
qxnJwz2rmooq0lbuahj8/LZ7a8ua7/V5j+umJ3bmedzJEw1X/O60uzWHi9v/ABlY
JlM=
=QR0X
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/braxen

### From the command line:

Consider the [keybase command line program](https://keybase.io/docs/command_line).

```bash
# look me up
keybase id braxen

# encrypt a message to me
keybase encrypt braxen -m 'a secret message...'

# ...and more...
```
